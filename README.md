Regression in Machine Learning. Tasks:

1 Simple regression

2 Polynomial regression and noise impact

3 Model choice

4 Regularized regression

5 Regularized polynomial regression

6 L1 and L2 regularization

7 Different-scaled features

8 Multicollinearity

Implemented in Python using NumPy, scikit-learn and Matplotlib library. Code and task description in the Jupyter Notebook.

My lab assignment in Machine Learning, FER, Zagreb.

Created: 2020
